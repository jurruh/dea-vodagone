package nl.jurrevriesen.vodagone.util;

public interface EncryptionStrategy {
    public String encrypt(String data) throws Exception;

    public String decrypt(String encryptedData) throws Exception;
}

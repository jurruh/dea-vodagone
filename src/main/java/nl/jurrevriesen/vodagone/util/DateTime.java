package nl.jurrevriesen.vodagone.util;

import java.util.Date;

public interface DateTime {
    Date getDate();
}

package nl.jurrevriesen.vodagone.util;

import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;

public class Sha256HashStrategy implements HashStrategy {

    public String hash(String password){

        return Hashing.sha256()
                .hashString(password, StandardCharsets.UTF_8)
                .toString();

    }

}

package nl.jurrevriesen.vodagone.util;

public interface HashStrategy {

    public String hash(String password);

}

package nl.jurrevriesen.vodagone.util;

import java.util.Date;

public class CurrentDateTime implements DateTime{

    @Override
    public Date getDate() {
        return new Date();
    }
}

package nl.jurrevriesen.vodagone.data;

import javax.enterprise.inject.Produces;
import javax.inject.Qualifier;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Qualifier
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD,
        ElementType.TYPE, ElementType.PARAMETER})
 @interface MySQLDatabase {}

public class EntityManagerProducer {

    private static EntityManager entityManager;

    /**
     * Use a singleton so that all the entities will persisted trough the same EntityManager
     * @return EntityManager
     */
    @Produces
    @MySQLDatabase
    public EntityManager getInstance() {
        if(entityManager == null){
            entityManager = Persistence
                    .createEntityManagerFactory("my-pu")
                    .createEntityManager();
        }

        return entityManager;
    }


}

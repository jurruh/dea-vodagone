package nl.jurrevriesen.vodagone.data;

import javax.inject.Inject;
import javax.persistence.*;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import java.util.List;

public abstract class BaseEntityDao<T> implements EntityDao<T> {

    protected EntityManager entityManager;

    @Inject
    public void setEntityManger(@MySQLDatabase EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public T find(int id) {
        return entityManager.find(getEntityClass(), id);
    }

    public List<T> findAll() {
        String jpql = "select e from " + getEntityName() + " e";
        return entityManager.createQuery(jpql).getResultList();
    }

    public void save(T entity) {
        beginTransaction();
        entityManager.persist(prepareEntity(entity));
        commitTransaction();
    }

    public void remove(int id) {
        T entity = find(id);
        beginTransaction();
        entityManager.remove(entity);
        commitTransaction();
    }

    private void beginTransaction() {
        try {
            entityManager.getTransaction().begin();
        } catch (IllegalStateException e) {
            rollBackTransaction();
        }
    }

    private void commitTransaction() {
        try {
            entityManager.getTransaction().commit();
        } catch (IllegalStateException | RollbackException e) {
            rollBackTransaction();
        }
    }

    private void rollBackTransaction() {
        try {
            entityManager.getTransaction().rollback();
        } catch (IllegalStateException | PersistenceException e) {
            e.printStackTrace();
        }
    }

    protected String getEntityName() {
        Metamodel meta = entityManager.getMetamodel();
        EntityType<T> entityType = meta.entity(getEntityClass());

        return entityType.getName();
    }

    protected T prepareEntity(T entity){
        return entity;
    }
    
    protected abstract Class<T> getEntityClass();

}
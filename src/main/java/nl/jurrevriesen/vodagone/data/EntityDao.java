package nl.jurrevriesen.vodagone.data;

import java.util.List;

public interface EntityDao<T> {

    T find(int id);

    List<T> findAll();


    void save(T entity);

    void remove(int id);
}
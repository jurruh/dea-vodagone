package nl.jurrevriesen.vodagone.data;

import nl.jurrevriesen.vodagone.data.entity.Abonnement;

import java.util.List;

public class AbonnementDao extends BaseEntityDao<Abonnement> implements  Filter<Abonnement>{

    @Override
    public Class<Abonnement> getEntityClass() {
        return Abonnement.class;
    }

    @Override
    public List<Abonnement> filter(String filter) {
        String jpql = "select e from " + getEntityName() + " e where lower(e.dienst) " +
                "like :filter or lower( e.aanbieder) like :filter";
        return entityManager.createQuery(jpql).setParameter("filter", "%" + filter.toLowerCase() + "%").getResultList();
    }
}

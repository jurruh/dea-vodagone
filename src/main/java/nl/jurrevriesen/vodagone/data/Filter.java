package nl.jurrevriesen.vodagone.data;

import java.util.List;

public interface Filter<T> {

    public List<T> filter(String filter);

}

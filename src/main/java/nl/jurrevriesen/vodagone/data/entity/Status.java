package nl.jurrevriesen.vodagone.data.entity;

public enum Status {
    OPGEZEGD, ACTIEF, PROEF
}

package nl.jurrevriesen.vodagone.data.entity;

import nl.jurrevriesen.vodagone.util.CurrentDateTime;
import nl.jurrevriesen.vodagone.util.DateTime;
import org.apache.commons.lang.time.DateUtils;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Entity
public class AbonneeAbonnement {

    @Transient
    private DateTime currentDateTime;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date datumAfgesloten;

    @Enumerated(EnumType.STRING)
    private Status status = Status.PROEF;

    @Enumerated(EnumType.STRING)
    private Verdubbeling verdubbeling = Verdubbeling.STANDAARD;


    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="abonnement")
    private Abonnement abonnement;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="abonnee")
    private Abonnee abonnee;

    @ManyToMany( targetEntity=Abonnee.class, cascade=CascadeType.PERSIST)
    @JoinTable(name = "GedeeldeAbonnees", joinColumns = @JoinColumn(name = "AbonneeAbonnement", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "Abonnee", referencedColumnName = "id"))
    private List<Abonnee> gedeeldeAbonnees = new ArrayList<Abonnee>();


    public AbonneeAbonnement(){
        this(new CurrentDateTime());
    }

    public AbonneeAbonnement(DateTime currentDateTime){
        this.currentDateTime = currentDateTime;
        datumAfgesloten = currentDateTime.getDate();
    }

    public int getId() {
        return id;
    }

    public Abonnement getAbonnement() {
        return abonnement;
    }

    public void setAbonnement(Abonnement abonnement) {
        this.abonnement = abonnement;
    }

    public Abonnee getAbonnee() {
        return abonnee;
    }

    public void setAbonnee(Abonnee abonnee) {
        this.abonnee = abonnee;
    }

    public Date getStartDatum(){
        return DateUtils.truncate(DateUtils.setDays(datumAfgesloten,1),Calendar.DATE);
    }

    public Status getStatus() {
        if(status == Status.PROEF && currentDateTime.getDate().after(DateUtils.addMonths(getStartDatum(),1))){
            status = Status.ACTIEF;
        }

        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Verdubbeling getVerdubbeling() {
        return verdubbeling;
    }

    public void setVerdubbeling(Verdubbeling verdubbeling) {
        this.verdubbeling = verdubbeling;
    }

    public void addGedeeldeAbonnee(Abonnee abonnee){
        gedeeldeAbonnees.add(abonnee);
    }

    public List<Abonnee> getGedeeldeAbonnees() {
        return gedeeldeAbonnees;
    }

    public Date getDatumAfgesloten() {
        return datumAfgesloten;
    }

    public void setDatumAfgesloten(Date datumAfgesloten) {
        this.datumAfgesloten = datumAfgesloten;
    }

    public double getPrijsPerMaand(){
        if(getStatus() == Status.ACTIEF){
            return abonnement.getPrijsPerMaand();
        }

        return 0;
    }

    public Boolean isVerdubbelingMogelijk(){
        return abonnement.isVerdubbelingMogelijk();
    }

    public boolean isDeelbaar(){
        return abonnement.isDeelbaar() && getGedeeldeAbonnees().size() < 2;
    }
}

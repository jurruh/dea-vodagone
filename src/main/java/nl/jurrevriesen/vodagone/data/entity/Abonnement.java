package nl.jurrevriesen.vodagone.data.entity;

import javax.persistence.*;

@Entity
public class Abonnement {

    @Id
    private int id;

    private String aanbieder;

    private String dienst;

    private double prijsPerMaand;

    private boolean deelbaar;

    private boolean verdubbelingMogelijk;

//    @OneToMany( targetEntity=AbonneeAbonnement.class, mappedBy = "abonnement")
//    private List<AbonneeAbonnement> abonnees;

    public Abonnement(){}


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAanbieder() {
        return aanbieder;
    }

    public void setAanbieder(String aanbieder) {
        this.aanbieder = aanbieder;
    }

    public String getDienst() {
        return dienst;
    }

    public void setDienst(String dienst) {
        this.dienst = dienst;
    }

    public double getPrijsPerMaand() {
        return prijsPerMaand;
    }

    public void setPrijsPerMaand(double prijsPerMaand) {
        this.prijsPerMaand = prijsPerMaand;
    }

    public void setDeelbaar(boolean deelbaar) {
        this.deelbaar = deelbaar;
    }

    public boolean isDeelbaar() {
        return deelbaar;
    }

    public void setVerdubbelingMogelijk(boolean verdubbelingMogelijk) {
        this.verdubbelingMogelijk = verdubbelingMogelijk;
    }

    public boolean isVerdubbelingMogelijk() {
        return verdubbelingMogelijk;
    }
}

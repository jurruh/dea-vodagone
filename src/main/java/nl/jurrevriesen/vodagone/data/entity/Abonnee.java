package nl.jurrevriesen.vodagone.data.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Abonnee {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    private String user;

    private String password;

    private String email;

    @OneToMany( targetEntity=AbonneeAbonnement.class, mappedBy = "abonnee",cascade=CascadeType.PERSIST)
    private List<AbonneeAbonnement> abonnementen = new ArrayList<AbonneeAbonnement>();

    @ManyToMany( targetEntity=AbonneeAbonnement.class,cascade=CascadeType.PERSIST, mappedBy = "gedeeldeAbonnees")
    private List<AbonneeAbonnement> gedeeldeAbonnementen = new ArrayList<AbonneeAbonnement>();


    public int getId() {
        return id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<AbonneeAbonnement> getAbonneeAbonnementen(){
        return abonnementen;
    }

    public void addAbonneeAbonnement(AbonneeAbonnement abonnement){
        abonnement.setAbonnee(this);
        this.abonnementen.add(abonnement);
    }

    public List<AbonneeAbonnement> getGedeeldeAbonnementen(){
        return gedeeldeAbonnementen;
    }

    public void addGedeeldAbonnement(AbonneeAbonnement abonnement){
        this.gedeeldeAbonnementen.add(abonnement);
    }

}

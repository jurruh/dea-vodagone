package nl.jurrevriesen.vodagone.data.entity;

public enum Verdubbeling {
    STANDAARD, VERDUBBELD, NIET_BESCHIKBAAR
}
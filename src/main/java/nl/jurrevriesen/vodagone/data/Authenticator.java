package nl.jurrevriesen.vodagone.data;

public interface Authenticator<T> {

    public abstract T findByUserNamePassword(String user, String password);

    public abstract T getFromToken(String token) throws UnauthorizedException;

    public abstract String getToken(T a);

}

package nl.jurrevriesen.vodagone.data;

import nl.jurrevriesen.vodagone.data.entity.AbonneeAbonnement;

public class AbonneeAbonnementDao extends BaseEntityDao<AbonneeAbonnement> {
    @Override
    protected Class<AbonneeAbonnement> getEntityClass() {
        return AbonneeAbonnement.class;
    }
}

package nl.jurrevriesen.vodagone.data;


import nl.jurrevriesen.vodagone.data.entity.Abonnee;
import nl.jurrevriesen.vodagone.util.EncryptionStrategy;
import nl.jurrevriesen.vodagone.util.HashStrategy;

import javax.inject.Inject;
import javax.persistence.Query;

import java.util.List;

public class AbonneeDao extends BaseEntityDao<Abonnee> implements Authenticator<Abonnee> {

    @Inject
    private EncryptionStrategy encryptionStrategy;

    @Inject
    private HashStrategy hashStrategy;

    @Override
    protected Class<Abonnee> getEntityClass() {
        return Abonnee.class;
    }

    @Override
    protected Abonnee prepareEntity(Abonnee entity) {
        Abonnee entityFromDatabase = find(entity.getId());

        //Only hash when password isn't already hashed
        if(entityFromDatabase.getPassword().equals(entity.getPassword())){
            entity.setPassword(hashStrategy.hash(entity.getPassword()));
        }

        return super.prepareEntity(entity);
    }

    public Abonnee findByUserNamePassword(String user, String password){
        String query = "select a from Abonnee a where a.user = :user" +
                " and a.password = :password";

        Query jpqlQuery = entityManager.createQuery(query)
                .setParameter("user", user)
                .setParameter("password", hashStrategy.hash(password));

        List<Abonnee> result = jpqlQuery.getResultList();

        for(Abonnee a : result){
            return a;
        }

        return null;
    }

    public Abonnee getFromToken(String token) throws UnauthorizedException {
        try {
            String decoded = encryptionStrategy.decrypt(token);

            Abonnee abonnee = find(Integer.parseInt(decoded));

            if(abonnee == null){
                throw new UnauthorizedException();
            }

            //Force refresh to be sure it is not cached
            entityManager.refresh(abonnee);

            return abonnee;

        } catch (Exception e){
            throw new UnauthorizedException();
        }

    }

    public String getToken(Abonnee a){
        try {
            return encryptionStrategy.encrypt(Integer.toString(a.getId()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public void setEncryptionStrategy(EncryptionStrategy encryptionStrategy) {
        this.encryptionStrategy = encryptionStrategy;
    }

    public void setHashStrategy(HashStrategy hashStrategy) {
        this.hashStrategy = hashStrategy;
    }
}

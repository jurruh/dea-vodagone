package nl.jurrevriesen.vodagone.service.auth;

import nl.jurrevriesen.vodagone.data.Authenticator;
import nl.jurrevriesen.vodagone.data.entity.Abonnee;
import nl.jurrevriesen.vodagone.data.UnauthorizedException;

import javax.annotation.Priority;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Provider
@Secured
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {

    @Inject
    @AuthenticatedUser
    Event<Abonnee> userAuthenticatedEvent;

    @Inject
    private Authenticator<Abonnee> abonneeDao;

    @Override
    public void filter(ContainerRequestContext requestContext) {
        String token = requestContext.getUriInfo().getQueryParameters().getFirst("token");

        if(token == null){
            requestContext.abortWith(Response.status(Response.Status.BAD_REQUEST).build());
        }else{
            try {
                Abonnee abonnee = abonneeDao.getFromToken(token);
                userAuthenticatedEvent.fire(abonnee);
            } catch (UnauthorizedException e) {
                requestContext.abortWith(Response.status(Response.Status.FORBIDDEN).build());
            }
        }
    }

    public void setUserAuthenticatedEvent(Event<Abonnee> userAuthenticatedEvent) {
        this.userAuthenticatedEvent = userAuthenticatedEvent;
    }

    public void setAbonneeDao(Authenticator<Abonnee> abonneeDao) {
        this.abonneeDao = abonneeDao;
    }
}
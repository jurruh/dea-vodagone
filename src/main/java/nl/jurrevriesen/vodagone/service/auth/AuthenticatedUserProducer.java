package nl.jurrevriesen.vodagone.service.auth;

import nl.jurrevriesen.vodagone.data.entity.Abonnee;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;

@RequestScoped
public class AuthenticatedUserProducer {

    @Produces
    @RequestScoped
    @AuthenticatedUser
    private Abonnee authenticatedUser;

    public void handleAuthenticationEvent(@Observes @AuthenticatedUser Abonnee abonnee) {
        this.authenticatedUser = abonnee;
    }

}
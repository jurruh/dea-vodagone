package nl.jurrevriesen.vodagone.service;

import nl.jurrevriesen.vodagone.data.Authenticator;
import nl.jurrevriesen.vodagone.data.entity.Abonnee;
import nl.jurrevriesen.vodagone.service.dto.LoginDto;
import nl.jurrevriesen.vodagone.service.dto.TokenDto;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("/login")
public class LoginService extends Application {

    @Inject
    private Authenticator<Abonnee> authenticator;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public TokenDto login(LoginDto login, @Context final HttpServletResponse response) {

        Abonnee a = authenticator.findByUserNamePassword(login.getUser(), login.getPassword());

        //Check if a user is found
        if(a == null){
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return null;
        }

        TokenDto result  = new TokenDto();

        result.setUser(a.getUser());
        result.setToken(authenticator.getToken(a));

        return result;
    }

    public void setAuthenticator(Authenticator<Abonnee> authenticator) {
        this.authenticator = authenticator;
    }
}


package nl.jurrevriesen.vodagone.service.dto;

import java.util.ArrayList;

public class AbonnementenDto {
    ArrayList<AbonnementDto> abonnementen = new ArrayList<AbonnementDto>();
    double totalPrice = 0.00;

    public ArrayList<AbonnementDto> getAbonnementen() {
        return abonnementen;
    }

    public void addAbonnement(AbonnementDto abonnement) {
        abonnementen.add(abonnement);
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }
}

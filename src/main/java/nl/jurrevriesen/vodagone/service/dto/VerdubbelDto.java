package nl.jurrevriesen.vodagone.service.dto;

public class VerdubbelDto {
    private String verdubbeling;

    public String getVerdubbeling() {
        return verdubbeling;
    }

    public void setVerdubbeling(String verdubbeling) {
        this.verdubbeling = verdubbeling;
    }
}

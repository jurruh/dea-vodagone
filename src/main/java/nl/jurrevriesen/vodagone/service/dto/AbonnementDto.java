package nl.jurrevriesen.vodagone.service.dto;

import nl.jurrevriesen.vodagone.data.entity.AbonneeAbonnement;
import nl.jurrevriesen.vodagone.data.entity.Abonnement;

public class AbonnementDto {

    private int id;
    private String dienst = "";
    private String aanbieder = "";


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDienst() {
        return dienst;
    }

    public void setDienst(String dienst) {
        this.dienst = dienst;
    }

    public String getAanbieder() {
        return aanbieder;
    }

    public void setAanbieder(String aanbieder) {
        this.aanbieder = aanbieder;
    }

    public static AbonnementDto createFromEntity(AbonneeAbonnement abonneeAbonnement){
        AbonnementDto dto = createFromEntity(abonneeAbonnement.getAbonnement());

        dto.setId(abonneeAbonnement.getId());

        return dto;
    }

    public static AbonnementDto createFromEntity(Abonnement abonnement){
        AbonnementDto abonnementDto = new AbonnementDto();

        abonnementDto.setId(abonnement.getId());
        abonnementDto.setAanbieder(abonnement.getAanbieder());
        abonnementDto.setDienst(abonnement.getDienst());

        return abonnementDto;
    }
}

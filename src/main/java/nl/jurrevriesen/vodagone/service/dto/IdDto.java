package nl.jurrevriesen.vodagone.service.dto;

public class IdDto {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

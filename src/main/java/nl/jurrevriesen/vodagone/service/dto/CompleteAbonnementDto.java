package nl.jurrevriesen.vodagone.service.dto;

import nl.jurrevriesen.vodagone.data.entity.AbonneeAbonnement;
import nl.jurrevriesen.vodagone.data.entity.Abonnement;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class CompleteAbonnementDto extends AbonnementDto {

    private String prijs;

    private String startDatum;

    private String verdubbeling;

    private boolean deelbaar;

    private String actief;

    private String status;

    public String getPrijs() {
        return prijs;
    }

    public void setPrijs(String prijs) {
        this.prijs = prijs;
    }

    public String getStartDatum() {
        return startDatum;
    }

    public void setStartDatum(String startDatum) {
        this.startDatum = startDatum;
    }

    public String getVerdubbeling() {
        return verdubbeling;
    }

    public void setVerdubbeling(String verdubbeling) {
        this.verdubbeling = verdubbeling;
    }

    public boolean isDeelbaar() {
        return deelbaar;
    }

    public void setDeelbaar(boolean deelbaar) {
        this.deelbaar = deelbaar;
    }

    public String getActief() {
        return actief;
    }

    public void setActief(String actief) {
        this.actief = actief;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static CompleteAbonnementDto createFromEntity(AbonneeAbonnement abonneeAbonnement){
        Abonnement abonnement = abonneeAbonnement.getAbonnement();
        CompleteAbonnementDto result = new CompleteAbonnementDto();

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        result.setId(abonneeAbonnement.getId());
        result.setAanbieder(abonnement.getAanbieder());
        result.setDienst(abonnement.getDienst());
        result.setPrijs("€" + String.format("%,.2f", abonneeAbonnement.getPrijsPerMaand()) + "- per maand");
        result.setStartDatum(dateFormat.format(abonneeAbonnement.getStartDatum()));
        if(abonneeAbonnement.isVerdubbelingMogelijk()){
            result.setVerdubbeling(abonneeAbonnement.getVerdubbeling().toString().toLowerCase().replace('_', '-'));
        }else{
            result.setVerdubbeling("niet-beschikbaar");
        }
        result.setDeelbaar(abonnement.isDeelbaar());
        result.setStatus(abonneeAbonnement.getStatus().toString().toLowerCase());

        return result;
    }
}

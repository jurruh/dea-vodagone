package nl.jurrevriesen.vodagone.service;

import nl.jurrevriesen.vodagone.data.Filter;
import nl.jurrevriesen.vodagone.data.entity.*;
import nl.jurrevriesen.vodagone.service.auth.AuthenticatedUser;
import nl.jurrevriesen.vodagone.service.auth.Secured;
import nl.jurrevriesen.vodagone.service.dto.AbonnementDto;
import nl.jurrevriesen.vodagone.data.EntityDao;
import nl.jurrevriesen.vodagone.service.dto.AbonnementenDto;
import nl.jurrevriesen.vodagone.service.dto.CompleteAbonnementDto;
import nl.jurrevriesen.vodagone.service.dto.VerdubbelDto;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("/abonnementen")
@Produces(MediaType.APPLICATION_JSON)
public class AbonnementenService extends Application {

    @Inject
    private EntityDao<Abonnement> abonnementDao;

    @Inject
    private Filter<Abonnement> abonnementFilter;

    @Inject
    private EntityDao<Abonnee> abonneeDao;

    @Inject
    private EntityDao<AbonneeAbonnement> abonneeAbonnementDao;

    @Inject
    @AuthenticatedUser
    Abonnee authenticatedAbonnee;

    @GET
    @Secured
    public AbonnementenDto abonnementen() {
        AbonnementenDto result = new AbonnementenDto();

        double totalPrice = 0;

        for (AbonneeAbonnement abonneeAbonnement : authenticatedAbonnee.getAbonneeAbonnementen()) {
            result.addAbonnement(AbonnementDto.createFromEntity(abonneeAbonnement));

            totalPrice += abonneeAbonnement.getPrijsPerMaand();
        }

        result.setTotalPrice(totalPrice);

        for(AbonneeAbonnement abonneeAbonnement : authenticatedAbonnee.getGedeeldeAbonnementen()){
            result.addAbonnement(AbonnementDto.createFromEntity(abonneeAbonnement));
        }

        return result;
    }

    @GET
    @Path("all")
    public List<AbonnementDto>  abonnementAll(@QueryParam("filter") String filter){
        List<AbonnementDto> result = new ArrayList<AbonnementDto>();

        for(Abonnement abonnement : abonnementFilter.filter(filter)){
            result.add(AbonnementDto.createFromEntity(abonnement));
        }

        return result;
    }

    @GET
    @Path("{id : \\d+}")
    public CompleteAbonnementDto abonnement(@PathParam("id") String id){
        AbonneeAbonnement abonneeAbonnement = abonneeAbonnementDao.find(Integer.parseInt(id));

        return CompleteAbonnementDto.createFromEntity(abonneeAbonnement);
    }

    @DELETE
    @Path("{id : \\d+}")
    public CompleteAbonnementDto deleteAbonnement(@PathParam("id") String id){
        AbonneeAbonnement abonneeAbonnement = abonneeAbonnementDao.find(Integer.parseInt(id));

        abonneeAbonnement.setStatus(Status.OPGEZEGD);
        abonneeAbonnementDao.save(abonneeAbonnement);

        return CompleteAbonnementDto.createFromEntity(abonneeAbonnement);
    }

    @POST
    @Path("{id : \\d+}")
    public CompleteAbonnementDto upgradeAbonnement(VerdubbelDto verdubbelDto, @PathParam("id") String id){
        AbonneeAbonnement abonneeAbonnement = abonneeAbonnementDao.find(Integer.parseInt(id));

        if(verdubbelDto.getVerdubbeling().equals("verdubbeld")){
            abonneeAbonnement.setVerdubbeling(Verdubbeling.VERDUBBELD);
            abonneeAbonnementDao.save(abonneeAbonnement);
        }

        return CompleteAbonnementDto.createFromEntity(abonneeAbonnement);
    }

    @POST
    @Secured
    @Consumes(MediaType.APPLICATION_JSON)
    public AbonnementenDto postAbonnement(AbonnementDto abonnementDto) {
        Abonnement abonnement = abonnementDao.find(abonnementDto.getId());

        AbonneeAbonnement abonneeAbonnement = new AbonneeAbonnement();
        abonneeAbonnement.setAbonnement(abonnement);

        authenticatedAbonnee.addAbonneeAbonnement(abonneeAbonnement);

        abonneeDao.save(authenticatedAbonnee);

        return abonnementen();

    }

    public void setAbonnementDao(EntityDao<Abonnement> abonnementDao) {
        this.abonnementDao = abonnementDao;
    }

    public void setAbonnementFilter(Filter<Abonnement> abonnementFilter) {
        this.abonnementFilter = abonnementFilter;
    }

    public void setAbonneeDao(EntityDao<Abonnee> abonneeDao) {
        this.abonneeDao = abonneeDao;
    }

    public void setAbonneeAbonnementDao(EntityDao<AbonneeAbonnement> abonneeAbonnementDao) {
        this.abonneeAbonnementDao = abonneeAbonnementDao;
    }

    public void setAuthenticatedAbonnee(Abonnee authenticatedAbonnee) {
        this.authenticatedAbonnee = authenticatedAbonnee;
    }
}

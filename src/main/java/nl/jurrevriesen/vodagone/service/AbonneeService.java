package nl.jurrevriesen.vodagone.service;

import nl.jurrevriesen.vodagone.data.EntityDao;
import nl.jurrevriesen.vodagone.data.entity.Abonnee;
import nl.jurrevriesen.vodagone.data.entity.AbonneeAbonnement;
import nl.jurrevriesen.vodagone.service.dto.AbonneeDto;
import nl.jurrevriesen.vodagone.service.dto.IdDto;


import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("/abonnees")
@Produces(MediaType.APPLICATION_JSON)
public class AbonneeService {

    @Inject
    private EntityDao<Abonnee> abonneeDao;

    @Inject
    private EntityDao<AbonneeAbonnement> aboneeAbonnementDao;

    @GET
    public List<AbonneeDto> abonnees(){
        List<AbonneeDto> result = new ArrayList<AbonneeDto>();

        for(Abonnee abonnee : abonneeDao.findAll()){
            result.add(AbonneeDto.createFromEntity(abonnee));
        }

        return result;
    }

    @POST
    @Path("{id : \\d+}")
    public void upgradeAbonnement(IdDto idDto, @PathParam("id") String id, @Context final HttpServletResponse response) {
        Abonnee abonnee = abonneeDao.find(Integer.parseInt(id));
        AbonneeAbonnement abonneeAbonnement = aboneeAbonnementDao.find(idDto.getId());

        if(!abonneeAbonnement.isDeelbaar()){
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }else{
            abonneeAbonnement.addGedeeldeAbonnee(abonnee);

            aboneeAbonnementDao.save(abonneeAbonnement);
        }
    }

    public void setAbonneeDao(EntityDao<Abonnee> abonneeDao) {
        this.abonneeDao = abonneeDao;
    }

    public void setAboneeAbonnementDao(EntityDao<AbonneeAbonnement> aboneeAbonnementDao) {
        this.aboneeAbonnementDao = aboneeAbonnementDao;
    }
}

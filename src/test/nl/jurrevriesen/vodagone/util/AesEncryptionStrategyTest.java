package nl.jurrevriesen.vodagone.util;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.fail;


public class AesEncryptionStrategyTest {

    AesEncryptionStrategy encryptionStrategy;

    @Before
    public void thatNewAbonneeAbonnementIsSetToCurrentAbonnee(){
        encryptionStrategy = new AesEncryptionStrategy();
    }

    @Test
    public void thatVariableAfterDecryptionIsTheSameAsBefore(){
        String value = "test encryption works";

        try {
            String encrypted = encryptionStrategy.encrypt(value);
            String decrypted = encryptionStrategy.decrypt(encrypted);

            assertEquals(value, decrypted);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void thatStringIsActuallyEncrypted(){
        String value = "this should not be the same";

        try {
            String encrypted = encryptionStrategy.encrypt(value);

            assertNotSame(value, encrypted);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

}
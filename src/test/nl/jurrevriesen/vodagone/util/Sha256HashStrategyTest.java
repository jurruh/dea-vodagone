package nl.jurrevriesen.vodagone.util;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class Sha256HashStrategyTest {

    HashStrategy hashStrategy;

    @Before
    public void setUp() {
        hashStrategy = new Sha256HashStrategy();
    }

    @Test
    public void thatHashIsSha256(){
        String hash = hashStrategy.hash("my hashed value");

        assertEquals("43d2a09034e6b7d567d4fe9cc2f0a02668e6d0587e82f9cc23932530a2651e42", hash);
    }

}
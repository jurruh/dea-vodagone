package nl.jurrevriesen.vodagone.data;

import nl.jurrevriesen.vodagone.data.entity.Abonnement;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;

public class AbonnementDaoTest {

    AbonnementDao abonnementDao;
    EntityManager entityManager;

    @Before
    public void setUp(){
        abonnementDao = new AbonnementDao();

        entityManager = Mockito.mock(EntityManager.class);

        abonnementDao.setEntityManger(entityManager);
    }

    @Test
    public void thatClassIsCorrect(){
        abonnementDao.find(1);

        Mockito.verify(entityManager).find(Abonnement.class, 1);
    }

    @Test
    public void thatFilterIsExecuted(){
        String jpql = "select e from Abonnement e where lower(e.dienst)" +
                " like :filter or lower( e.aanbieder) like :filter";
        String filter = "my filter";

        Query query = Mockito.mock(Query.class);
        Metamodel metamodel = Mockito.mock(Metamodel.class);
        EntityType<Abonnement> entityType = Mockito.mock(EntityType.class);

        Mockito.when(entityManager.getMetamodel()).thenReturn(metamodel);
        Mockito.when(metamodel.entity(Abonnement.class)).thenReturn(entityType);
        Mockito.when(entityType.getName()).thenReturn("Abonnement");
        Mockito.when(query.setParameter("filter","%" + filter + "%")).thenReturn(query);
        Mockito.when(entityManager.createQuery(jpql)).thenReturn(query);

        abonnementDao.filter(filter);

        Mockito.verify(query).setParameter("filter","%" + filter + "%");
        Mockito.verify(entityManager).createQuery(jpql);

    }



}
package nl.jurrevriesen.vodagone.data.entity;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;

public class AbonneeTest {

    Abonnee abonnee;

    @Before
    public void setUp(){
        abonnee = new Abonnee();
    }

    @Test
    public void thatNewAbonneeAbonnementIsSetToCurrentAbonnee(){
        AbonneeAbonnement abonneeAbonnement = Mockito.mock(AbonneeAbonnement.class);
        abonnee.addAbonneeAbonnement(abonneeAbonnement);

        Mockito.verify(abonneeAbonnement, Mockito.times(1)).setAbonnee(abonnee);
    }

    @Test
    public void thatRemainingSettersAndGettersWork(){
        assertEquals(abonnee.getId(), 0);

        abonnee.setUser("user");
        assertEquals(abonnee.getUser(), "user");

        abonnee.setPassword("password");
        assertEquals(abonnee.getPassword(), "password");

        AbonneeAbonnement abonneeAbonnement = Mockito.mock(AbonneeAbonnement.class);

        abonnee.addAbonneeAbonnement(abonneeAbonnement);
        assertTrue(abonnee.getAbonneeAbonnementen().contains(abonneeAbonnement));

        abonnee.addGedeeldAbonnement(abonneeAbonnement);
        assertTrue(abonnee.getGedeeldeAbonnementen().contains(abonneeAbonnement));
    }

}
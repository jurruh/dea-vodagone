package nl.jurrevriesen.vodagone.data.entity;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AbonnementTest {

    Abonnement abonnement;

    @Before
    public void setUp(){
        abonnement = new Abonnement();
    }

    @Test
    public void thatRemainingSettersAndGettersWork(){
        assertEquals(0, abonnement.getId());

        abonnement.setAanbieder("aanbieder");
        assertEquals("aanbieder", abonnement.getAanbieder());

        abonnement.setDienst("dienst");
        assertEquals("dienst", abonnement.getDienst());

        abonnement.setPrijsPerMaand(10.0);
        assertEquals(10, abonnement.getPrijsPerMaand(), 0.1);

        abonnement.setDeelbaar(true);
        assertTrue(abonnement.isDeelbaar());

        abonnement.setVerdubbelingMogelijk(true);
        assertTrue(abonnement.isVerdubbelingMogelijk());
    }

}
package nl.jurrevriesen.vodagone.data.entity;

import nl.jurrevriesen.vodagone.util.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class AbonneeAbonnementTest {

    private AbonneeAbonnement abonneeAbonnement;
    private SimpleDateFormat dateFormatter;

    @Before
    public void setUp() throws ParseException {
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date currentDate = dateFormatter.parse("2018-03-22 21:23:01");
        final DateTime curentDateTimeMock = Mockito.mock(DateTime.class);
        Mockito.when(curentDateTimeMock.getDate()).thenReturn(currentDate);
        abonneeAbonnement = new AbonneeAbonnement(curentDateTimeMock);

    }

    @Test
    public void thatStartDatumStartsOnFirstOfMonth() throws ParseException {
        Date datumAfgesloten = dateFormatter.parse("2014-10-05 15:23:01");
        abonneeAbonnement.setDatumAfgesloten(datumAfgesloten);
        String startDatum = dateFormatter.format(abonneeAbonnement.getStartDatum());
        assertEquals(startDatum, "2014-10-01 00:00:00");
    }

    @Test
    public void thatStartDatumStartsOnFirstOfMonthLeapDay() throws ParseException {
        Date datumAfgesloten = dateFormatter.parse("2012-02-29 15:23:01");
        abonneeAbonnement.setDatumAfgesloten(datumAfgesloten);
        String startDatum = dateFormatter.format(abonneeAbonnement.getStartDatum());
        assertEquals(startDatum, "2012-02-01 00:00:00");
    }

    @Test
    public void thatStatusIsProefInFirstMonth() throws ParseException {
        Date datumAfgesloten = dateFormatter.parse("2018-03-21 20:12:01");
        abonneeAbonnement.setDatumAfgesloten(datumAfgesloten);

        assertEquals(Status.PROEF, abonneeAbonnement.getStatus());
        abonneeAbonnement.getStatus();
    }

    @Test
    public void thatStatusIsActiefAfterOneMonth() throws ParseException {
        Date datumAfgesloten = dateFormatter.parse("2018-02-21 20:12:01");
        abonneeAbonnement.setDatumAfgesloten(datumAfgesloten);

        assertEquals(Status.ACTIEF, abonneeAbonnement.getStatus());
        abonneeAbonnement.getStatus();
    }

    @Test
    public void thatPrijsPerMaandIs0InProefPeriod() throws ParseException {
        Date datumAfgesloten = dateFormatter.parse("2018-03-21 20:12:01");
        abonneeAbonnement.setDatumAfgesloten(datumAfgesloten);

        assertEquals(0, abonneeAbonnement.getPrijsPerMaand(), 0.1);
    }

    @Test
    public void thatPrijsPerMaandIsNot0AfterProefPeriod() throws ParseException {
        Date datumAfgesloten = dateFormatter.parse("2018-02-21 20:12:01");

        final Abonnement abonnement = Mockito.mock(Abonnement.class);
        Mockito.when(abonnement.getPrijsPerMaand()).thenReturn(10.0);
        abonneeAbonnement.setAbonnement(abonnement);

        abonneeAbonnement.setDatumAfgesloten(datumAfgesloten);

        assertEquals(10.0, abonneeAbonnement.getPrijsPerMaand(), 0.1);
    }

    @Test
    public void thatPrijsPerMaandIs0WhenOpgezegd() {
        abonneeAbonnement.setStatus(Status.OPGEZEGD);

        assertEquals(0, abonneeAbonnement.getPrijsPerMaand(), 0.1);
    }

    @Test
    public void thatAbonnementIsDeelbaar() {
        Abonnement abonnement = Mockito.mock(Abonnement.class);
        Mockito.when(abonnement.isDeelbaar()).thenReturn(true);
        abonneeAbonnement.setAbonnement(abonnement);

        assertTrue(abonneeAbonnement.isDeelbaar());
    }

    @Test
    public void thatAbonnementIsNotDeelbaarWhenAlreadyHasTwoShared() {
        Abonnement abonnement = Mockito.mock(Abonnement.class);
        Mockito.when(abonnement.isDeelbaar()).thenReturn(true);
        abonneeAbonnement.setAbonnement(abonnement);

        Abonnee abonnee1 = Mockito.mock(Abonnee.class);
        Abonnee abonnee2 = Mockito.mock(Abonnee.class);

        abonneeAbonnement.addGedeeldeAbonnee(abonnee1);
        abonneeAbonnement.addGedeeldeAbonnee(abonnee2);

        assertFalse(abonneeAbonnement.isDeelbaar());
    }

    @Test
    public void thatAbonnementIsDeelbaarWhenHasOneShared() {
        Abonnement abonnement = Mockito.mock(Abonnement.class);
        Mockito.when(abonnement.isDeelbaar()).thenReturn(true);
        abonneeAbonnement.setAbonnement(abonnement);

        Abonnee abonnee1 = Mockito.mock(Abonnee.class);

        abonneeAbonnement.addGedeeldeAbonnee(abonnee1);

        assertTrue(abonneeAbonnement.isDeelbaar());
    }

    @Test
    public void thatRemainingSettersAndGettersWork() {
        Abonnee abonnee = Mockito.mock(Abonnee.class);
        Abonnement abonnement = Mockito.mock(Abonnement.class);

        assertEquals(0, abonneeAbonnement.getId());

        abonneeAbonnement.setAbonnement(abonnement);
        assertEquals(abonnement, abonneeAbonnement.getAbonnement());

        abonneeAbonnement.setAbonnee(abonnee);
        assertEquals(abonnee, abonneeAbonnement.getAbonnee());

        abonneeAbonnement.setVerdubbeling(Verdubbeling.STANDAARD);
        assertEquals(abonneeAbonnement.getVerdubbeling(), Verdubbeling.STANDAARD);

        abonneeAbonnement.addGedeeldeAbonnee(abonnee);
        assertTrue(abonneeAbonnement.getGedeeldeAbonnees().contains(abonnee));

        Date afgesloten = new Date();
        abonneeAbonnement.setDatumAfgesloten(afgesloten);
        assertEquals(abonneeAbonnement.getDatumAfgesloten(), afgesloten);
    }
}
package nl.jurrevriesen.vodagone.data;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import java.util.List;

import static org.junit.Assert.*;

class BaseEntityDaoMock extends BaseEntityDao<Object> {

    @Override
    protected Class<Object> getEntityClass() {
        return Object.class;
    }

    @Override
    protected String getEntityName(){
        return "my_name";
    }
}

public class BaseEntityDaoTest {

    BaseEntityDao<Object> baseEntityDao;
    EntityManager entityManager;
    EntityTransaction transaction;

    @Before
    public void setUp(){
        entityManager = Mockito.mock(EntityManager.class);
        baseEntityDao = new BaseEntityDaoMock();

        transaction = Mockito.mock(EntityTransaction.class);
        Mockito.when(entityManager.getTransaction()).thenReturn(transaction);

        baseEntityDao.setEntityManger(entityManager);
    }

    @Test
    public void thatEntityIsFound(){
        Object expected = new Object();

        Mockito.when(entityManager.find(Object.class, 1)).thenReturn(expected);

        assertEquals(expected, baseEntityDao.find(1));
    }

    @Test
    public void thatFindAllQueryReturnsExpected(){
        Query query = Mockito.mock(Query.class);
        List<Object> expected = Mockito.mock(List.class);

        Mockito.when(entityManager.createQuery("select e from my_name e")).thenReturn(query);
        Mockito.when(query.getResultList()).thenReturn(expected);

        assertEquals(expected, baseEntityDao.findAll());
        Mockito.verify(entityManager).createQuery("select e from my_name e");
    }

    @Test
    public void thatSaveEntityTransactionBeginCommitIsInOrder(){
        baseEntityDao.save(new Object());

        InOrder inOrder = Mockito.inOrder(transaction);
        inOrder.verify(transaction).begin();
        inOrder.verify(transaction).commit();
    }

    @Test
    public void thatEntityIsPersisted(){
        Object o = new Object();

        baseEntityDao.save(o);

        Mockito.verify(entityManager).persist(o);
    }


    @Test
    public void thatRemoveEntityTransactionBeginCommitIsInOrder(){
        baseEntityDao.remove(1);

        InOrder inOrder = Mockito.inOrder(transaction);
        inOrder.verify(transaction).begin();
        inOrder.verify(transaction).commit();
    }

    @Test
    public void thatEntityIsRemoved(){
        Object expected = new Object();

        Mockito.when(entityManager.find(Object.class, 1)).thenReturn(expected);

        baseEntityDao.remove(1);

        Mockito.verify(entityManager).remove(expected);
    }

}
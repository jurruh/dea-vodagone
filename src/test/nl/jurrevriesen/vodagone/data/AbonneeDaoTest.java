package nl.jurrevriesen.vodagone.data;

import nl.jurrevriesen.vodagone.data.entity.Abonnee;
import nl.jurrevriesen.vodagone.util.EncryptionStrategy;
import nl.jurrevriesen.vodagone.util.HashStrategy;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class AbonneeDaoTest {

    AbonneeDao abonneeDao;
    EntityManager entityManager;
    HashStrategy hashStrategy;
    EncryptionStrategy encryptionStrategy;

    @Before
    public void setUp() {
        abonneeDao = new AbonneeDao();

        entityManager = Mockito.mock(EntityManager.class);
        encryptionStrategy = Mockito.mock(EncryptionStrategy.class);
        hashStrategy = Mockito.mock(HashStrategy.class);

        abonneeDao.setEntityManger(entityManager);
        abonneeDao.setEncryptionStrategy(encryptionStrategy);
        abonneeDao.setHashStrategy(hashStrategy);
    }

    @Test
    public void thatClassIsCorrect() {
        abonneeDao.find(1);

        Mockito.verify(entityManager).find(Abonnee.class, 1);
    }

    @Test
    public void thatAbonneeIsFoundByUserNameAndHashedPassword() {
        Query query = Mockito.mock(Query.class);

        String password = "my_password";
        String user = "my_user";
        String hashedPassword = "1234124513245";

        Mockito.when(entityManager.createQuery("select a from Abonnee a where a.user = :user" +
                " and a.password = :password")).thenReturn(query);
        Mockito.when(query.setParameter("user", user)).thenReturn(query);
        Mockito.when(hashStrategy.hash(password)).thenReturn(hashedPassword);
        Mockito.when(query.setParameter("password", hashedPassword)).thenReturn(query);

        List<Abonnee> list = new ArrayList<Abonnee>();
        Abonnee abonnee = Mockito.mock(Abonnee.class);
        list.add(abonnee);

        Mockito.when(query.getResultList()).thenReturn(list);

        abonneeDao.findByUserNamePassword(user, password);
    }

    @Test
    public void thatAbonneeIsFoundFromToken() throws Exception {
        String token = "my_token";

        Mockito.when(encryptionStrategy.decrypt(token)).thenReturn("1");


        Abonnee abonnee = Mockito.mock(Abonnee.class);
        Mockito.when(entityManager.find(Abonnee.class, 1)).thenReturn(abonnee);

        assertEquals(abonnee,abonneeDao.getFromToken(token));

        Mockito.verify(entityManager).find(Abonnee.class, 1);

    }

    @Test(expected = UnauthorizedException.class)
    public void thatAbonneeExceptionIsThrowedWhenIsFoundFromToken() throws UnauthorizedException{
        String token = "my_token";

        try {
            Mockito.when(encryptionStrategy.decrypt(token)).thenReturn("1");
        } catch (Exception e) {
            fail();
        }

        Mockito.when(entityManager.find(Abonnee.class, 1)).thenReturn(null);

        abonneeDao.getFromToken(token);

        fail();
    }

    @Test
    public void thatTokenIsCreatedFromAbonnee() throws Exception{
        Mockito.when(encryptionStrategy.encrypt("1")).thenReturn("1_encrypted");

        Abonnee abonnee = Mockito.mock(Abonnee.class);
        Mockito.when(abonnee.getId()).thenReturn(1);

        assertEquals("1_encrypted", abonneeDao.getToken(abonnee));
    }


}
package nl.jurrevriesen.vodagone.data;

import nl.jurrevriesen.vodagone.data.entity.AbonneeAbonnement;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.persistence.EntityManager;

public class AbonneeAbonnementDaoTest {

    AbonneeAbonnementDao abonneeAbonnementDao;
    EntityManager entityManager;

    @Before
    public void setUp(){
        abonneeAbonnementDao = new AbonneeAbonnementDao();

        entityManager = Mockito.mock(EntityManager.class);

        abonneeAbonnementDao.setEntityManger(entityManager);
    }

    @Test
    public void thatClassIsCorrect(){
        abonneeAbonnementDao.find(1);

        Mockito.verify(entityManager).find(AbonneeAbonnement.class, 1);
    }

}
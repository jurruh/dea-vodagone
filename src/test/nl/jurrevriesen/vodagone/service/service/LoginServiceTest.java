package nl.jurrevriesen.vodagone.service.service;

import nl.jurrevriesen.vodagone.data.Authenticator;
import nl.jurrevriesen.vodagone.data.entity.Abonnee;
import nl.jurrevriesen.vodagone.service.LoginService;
import nl.jurrevriesen.vodagone.service.dto.LoginDto;
import nl.jurrevriesen.vodagone.service.dto.TokenDto;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletResponse;

import static org.junit.Assert.*;

public class LoginServiceTest {

    LoginService service;

    Authenticator<Abonnee> authenticator;

    @Before
    public void setUp(){
        service = new LoginService();

        authenticator = Mockito.mock(Authenticator.class);
        service.setAuthenticator(authenticator);
    }

    @Test
    public void thatUnauthorizedWhenNoUserIsFound(){
        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

        LoginDto dto = new LoginDto();
        dto.setUser("my_user");
        dto.setUser("my_password");

        Mockito.when(authenticator.findByUserNamePassword("my_user", "my_password")).thenReturn(null);

        service.login(dto, response);

        Mockito.verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }

    @Test
    public void thatUserAndTokenAreReturned(){
        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

        LoginDto dto = new LoginDto();
        dto.setUser("my_user");
        dto.setPassword("my_password");

        Abonnee abonnee = Mockito.mock(Abonnee.class);

        Mockito.when(authenticator.findByUserNamePassword("my_user", "my_password")).thenReturn(abonnee);
        Mockito.when(abonnee.getUser()).thenReturn("my_user");
        Mockito.when(authenticator.getToken(abonnee)).thenReturn("my_token");

        TokenDto resultDto = service.login(dto, response);

        assertEquals(resultDto.getToken(), "my_token");
        assertEquals(resultDto.getUser(), "my_user");
    }
}
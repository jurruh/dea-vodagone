package nl.jurrevriesen.vodagone.service.service;

import nl.jurrevriesen.vodagone.data.EntityDao;
import nl.jurrevriesen.vodagone.data.entity.Abonnee;
import nl.jurrevriesen.vodagone.data.entity.AbonneeAbonnement;
import nl.jurrevriesen.vodagone.service.AbonneeService;
import nl.jurrevriesen.vodagone.service.dto.AbonneeDto;
import nl.jurrevriesen.vodagone.service.dto.IdDto;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class AbonneeServiceTest {

    AbonneeService service;
    EntityDao<Abonnee> abonneeDao;
    EntityDao<AbonneeAbonnement> aboneeAbonnementDao;


    @Before
    public void setUp(){
        service = new AbonneeService();

        abonneeDao = Mockito.mock(EntityDao.class);
        service.setAbonneeDao(abonneeDao);

        aboneeAbonnementDao = Mockito.mock(EntityDao.class);
        service.setAboneeAbonnementDao(aboneeAbonnementDao);
    }


    @Test
    public void thatAbonneesAreReturned(){
        Abonnee abonnee = Mockito.mock(Abonnee.class);

        List<Abonnee> list = new ArrayList<>();
        list.add(abonnee);

        Mockito.when(abonneeDao.findAll()).thenReturn(list);
        Mockito.when(abonnee.getId()).thenReturn(1);

        List<AbonneeDto> dto = service.abonnees();

        assertEquals(dto.get(0).getId(), 1);
    }

    @Test
    public void thatAbonnementIsUpgraded(){
        Abonnee abonnee = Mockito.mock(Abonnee.class);
        IdDto idDto = Mockito.mock(IdDto.class);
        AbonneeAbonnement abonneeAbonnement = Mockito.mock(AbonneeAbonnement.class);

        Mockito.when(idDto.getId()).thenReturn(1);
        Mockito.when(abonneeDao.find(0)).thenReturn(abonnee);
        Mockito.when(aboneeAbonnementDao.find(1)).thenReturn(abonneeAbonnement);
        Mockito.when(abonneeAbonnement.isDeelbaar()).thenReturn(true);

        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

        service.upgradeAbonnement(idDto, "0", response);

        Mockito.verify(abonneeAbonnement).addGedeeldeAbonnee(abonnee);
        Mockito.verify(aboneeAbonnementDao).save(abonneeAbonnement);
    }

    @Test
    public void thatBadRequestWhenAbonnementNotDeelbaar(){
        IdDto idDto = Mockito.mock(IdDto.class);
        AbonneeAbonnement abonneeAbonnement = Mockito.mock(AbonneeAbonnement.class);

        Mockito.when(idDto.getId()).thenReturn(1);
        Mockito.when(aboneeAbonnementDao.find(1)).thenReturn(abonneeAbonnement);
        Mockito.when(abonneeAbonnement.isDeelbaar()).thenReturn(false);

        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

        service.upgradeAbonnement(idDto, "0", response);

        Mockito.verify(response).setStatus(HttpServletResponse.SC_BAD_REQUEST);
    }

}
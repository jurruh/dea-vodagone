package nl.jurrevriesen.vodagone.service.service;

import nl.jurrevriesen.vodagone.data.EntityDao;
import nl.jurrevriesen.vodagone.data.Filter;
import nl.jurrevriesen.vodagone.data.entity.*;
import nl.jurrevriesen.vodagone.service.AbonnementenService;
import nl.jurrevriesen.vodagone.service.dto.AbonnementDto;
import nl.jurrevriesen.vodagone.service.dto.AbonnementenDto;
import nl.jurrevriesen.vodagone.service.dto.CompleteAbonnementDto;
import nl.jurrevriesen.vodagone.service.dto.VerdubbelDto;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class AbonnementenServiceTest {

    AbonnementenService service;
    EntityDao<Abonnee> abonneeDao;
    EntityDao<Abonnement> abonnementDao;
    Filter<Abonnement> abonnementFilter;
    EntityDao<AbonneeAbonnement> abonneeAbonnementDao;
    Abonnee authenticatedAbonnee;

    @Before
    public void setUp(){
        service = new AbonnementenService();

        abonneeDao = Mockito.mock(EntityDao.class);
        service.setAbonneeDao(abonneeDao);

        abonnementFilter = Mockito.mock(Filter.class);
        service.setAbonnementFilter(abonnementFilter);

        abonneeAbonnementDao = Mockito.mock(EntityDao.class);
        service.setAbonneeAbonnementDao(abonneeAbonnementDao);

        abonnementDao = Mockito.mock(EntityDao.class);
        service.setAbonnementDao(abonnementDao);

        authenticatedAbonnee = Mockito.mock(Abonnee.class);
        service.setAuthenticatedAbonnee(authenticatedAbonnee);
    }

    @Test
    public void thatTotalPriceIsCalculatedFromOwnAbonnementen(){
        AbonneeAbonnement abonnement1 = Mockito.mock(AbonneeAbonnement.class);
        Mockito.when(abonnement1.getPrijsPerMaand()).thenReturn(20.0);
        Mockito.when(abonnement1.getAbonnement()).thenReturn(new Abonnement());

        AbonneeAbonnement abonnement2 = Mockito.mock(AbonneeAbonnement.class);
        Mockito.when(abonnement2.getPrijsPerMaand()).thenReturn(10.0);
        Mockito.when(abonnement2.getAbonnement()).thenReturn(new Abonnement());

        List<AbonneeAbonnement> abonnementen = new ArrayList<AbonneeAbonnement>();
        abonnementen.add(abonnement1);
        abonnementen.add(abonnement2);

        Mockito.when(authenticatedAbonnee.getAbonneeAbonnementen()).thenReturn(abonnementen);

        AbonnementenDto dto = service.abonnementen();

        assertEquals(dto.getTotalPrice(), 30.0, 0.1);
    }

    @Test
    public void thatTotalPriceIsNotCalculatedFromSharedAbonnementen(){
        AbonneeAbonnement abonnement1 = Mockito.mock(AbonneeAbonnement.class);
        Mockito.when(abonnement1.getPrijsPerMaand()).thenReturn(20.0);
        Mockito.when(abonnement1.getAbonnement()).thenReturn(new Abonnement());

        List<AbonneeAbonnement> abonnementen = new ArrayList<AbonneeAbonnement>();
        abonnementen.add(abonnement1);

        Mockito.when(authenticatedAbonnee.getGedeeldeAbonnementen()).thenReturn(abonnementen);

        AbonnementenDto dto = service.abonnementen();

        assertEquals(dto.getTotalPrice(), 0.0, 0.1);
    }

    @Test
    public void thatAbonnementFilterIsCalled(){
        List<Abonnement> list = new ArrayList<Abonnement>();

        Abonnement abonnement = new Abonnement();
        abonnement.setId(1);
        list.add(abonnement);

        Mockito.when(abonnementFilter.filter("my_filter")).thenReturn(list);

        List<AbonnementDto> dto = service.abonnementAll("my_filter");

        Mockito.verify(abonnementFilter).filter("my_filter");

        assertEquals(1, dto.get(0).getId());
    }
    
    @Test
    public void thatAbonnementCanBeFoundById(){
        AbonneeAbonnement abonneeAbonnement = new AbonneeAbonnement();
        Abonnement abonnement = new Abonnement();
        abonnement.setDienst("mijn_dienst");
        abonneeAbonnement.setAbonnement(abonnement);

        Mockito.when(abonneeAbonnementDao.find(1)).thenReturn(abonneeAbonnement);

        CompleteAbonnementDto dto = service.abonnement("1");

        assertEquals("mijn_dienst", dto.getDienst());
    }

    @Test
    public void thatAbonnementIsOpgezegd(){
        AbonneeAbonnement abonneeAbonnement = new AbonneeAbonnement();
        Abonnement abonnement = new Abonnement();
        abonneeAbonnement.setAbonnement(abonnement);

        Mockito.when(abonneeAbonnementDao.find(1)).thenReturn(abonneeAbonnement);
        CompleteAbonnementDto dto = service.deleteAbonnement("1");

        assertEquals(Status.OPGEZEGD, abonneeAbonnement.getStatus());
        assertEquals("opgezegd", dto.getStatus());
    }

    @Test
    public void thatCanUpgradeAddAbonnement(){
        AbonneeAbonnement abonneeAbonnement = new AbonneeAbonnement();
        Abonnement abonnement = new Abonnement();
        abonnement.setVerdubbelingMogelijk(true);
        abonneeAbonnement.setAbonnement(abonnement);
        abonneeAbonnement.setVerdubbeling(Verdubbeling.VERDUBBELD);

        Mockito.when(abonneeAbonnementDao.find(1)).thenReturn(abonneeAbonnement);

        VerdubbelDto postDto = new VerdubbelDto();
        postDto.setVerdubbeling("verdubbeld");

        CompleteAbonnementDto resultDto = service.upgradeAbonnement(postDto,"1");

        Mockito.verify(abonneeAbonnementDao).save(abonneeAbonnement);
        assertEquals("verdubbeld",resultDto.getVerdubbeling() );
    }

    @Test
    public void thatCanAddAbonnement(){
        AbonnementDto dto = new AbonnementDto();
        dto.setId(1);

        Abonnement abonnement = Mockito.mock(Abonnement.class);
        Mockito.when(abonnementDao.find(1)).thenReturn(abonnement);
        ArgumentCaptor<AbonneeAbonnement> captor = ArgumentCaptor.forClass(AbonneeAbonnement.class);

        service.postAbonnement(dto);

        Mockito.verify(authenticatedAbonnee).addAbonneeAbonnement(captor.capture());
        Abonnement added = captor.getValue().getAbonnement();

        assertEquals(abonnement, added);
    }

}
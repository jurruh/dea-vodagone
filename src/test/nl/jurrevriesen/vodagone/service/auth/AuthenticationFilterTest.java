package nl.jurrevriesen.vodagone.service.auth;

import nl.jurrevriesen.vodagone.data.Authenticator;
import nl.jurrevriesen.vodagone.data.UnauthorizedException;
import nl.jurrevriesen.vodagone.data.entity.Abonnee;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.enterprise.event.Event;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

public class AuthenticationFilterTest {

    Authenticator abonneeDao;
    AuthenticationFilter authenticationFilter;
    @Before
    public void setUp(){
        abonneeDao = Mockito.mock(Authenticator.class);

        authenticationFilter = new AuthenticationFilter();
        authenticationFilter.setAbonneeDao(abonneeDao);
    }

    @Test
    public void thatAbonneeIsFiredToEvent() throws UnauthorizedException{
        Event<Abonnee> userAuthenticatedEvent = Mockito.mock(Event.class);
        Abonnee abonnee = Mockito.mock(Abonnee.class);

        authenticationFilter.setUserAuthenticatedEvent(userAuthenticatedEvent);

        UriInfo uriInfo = Mockito.mock(UriInfo.class);
        MultivaluedMap<String, String> multiValueMap = Mockito.mock(MultivaluedMap.class);
        ContainerRequestContext requestContext = Mockito.mock(ContainerRequestContext.class);

        Mockito.when(requestContext.getUriInfo()).thenReturn(uriInfo);
        Mockito.when(uriInfo.getQueryParameters()).thenReturn(multiValueMap);
        Mockito.when(multiValueMap.getFirst("token")).thenReturn("my_token");

        Mockito.when(abonneeDao.getFromToken("my_token")).thenReturn(abonnee);

        authenticationFilter.filter(requestContext);

        Mockito.verify(userAuthenticatedEvent).fire(abonnee);
    }

}
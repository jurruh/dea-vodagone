package nl.jurrevriesen.vodagone.service.dto;

import org.junit.Test;

import static org.junit.Assert.*;

public class VerdubbelDtoTest {

    @Test
    public void thatGettersAndSettersWork(){
        VerdubbelDto dto = new VerdubbelDto();
        dto.setVerdubbeling("my_verdubbeling");

        assertEquals("my_verdubbeling", dto.getVerdubbeling());
    }

}
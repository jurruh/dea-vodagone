package nl.jurrevriesen.vodagone.service.dto;

import org.junit.Test;

import static org.junit.Assert.*;

public class LoginDtoTest {

    @Test
    public void thatGettersAndSettersWork(){
        LoginDto dto = new LoginDto();

        dto.setPassword("my_password");
        dto.setUser("my_user");

        assertEquals("my_password", dto.getPassword());
        assertEquals("my_user", dto.getUser());
    }

}
package nl.jurrevriesen.vodagone.service.dto;

import org.junit.Test;

import static org.junit.Assert.*;

public class IdDtoTest {
    @Test
    public void thatGettersAndSettersWork() {
        IdDto dto = new IdDto();

        dto.setId(1);
        assertEquals(1, dto.getId());
    }

}
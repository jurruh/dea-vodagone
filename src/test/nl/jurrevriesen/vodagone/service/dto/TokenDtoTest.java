package nl.jurrevriesen.vodagone.service.dto;

import org.junit.Test;

import static org.junit.Assert.*;

public class TokenDtoTest {
    @Test
    public void thatGettersAndSettersWork() {
        TokenDto dto = new TokenDto();
        dto.setToken("my_token");
        dto.setUser("my_user");

        assertEquals("my_token",dto.getToken());
        assertEquals("my_user",dto.getUser());
    }

}
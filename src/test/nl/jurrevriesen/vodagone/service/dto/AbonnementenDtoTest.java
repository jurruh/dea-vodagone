package nl.jurrevriesen.vodagone.service.dto;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;

public class AbonnementenDtoTest {

    @Test
    public void thatGettersAndSettersWork(){
        AbonnementenDto dto = new AbonnementenDto();
        dto.setTotalPrice(10.00);
        assertEquals(10.00, dto.getTotalPrice(), 0.1);

        AbonnementDto abonnement = Mockito.mock(AbonnementDto.class);
        dto.addAbonnement(abonnement);
        assertTrue(dto.getAbonnementen().contains(abonnement));
    }
}
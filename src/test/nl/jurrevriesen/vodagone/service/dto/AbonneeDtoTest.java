package nl.jurrevriesen.vodagone.service.dto;

import nl.jurrevriesen.vodagone.data.entity.Abonnee;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;

public class AbonneeDtoTest {

    @Test
    public void thatCanCreateFromEntity(){

        Abonnee abonnee = Mockito.mock(Abonnee.class);

        Mockito.when(abonnee.getId()).thenReturn(1);
        Mockito.when(abonnee.getEmail()).thenReturn("test@test.nl");
        Mockito.when(abonnee.getUser()).thenReturn("my_user");

        AbonneeDto dto = AbonneeDto.createFromEntity(abonnee);

        assertEquals(1, dto.getId());
        assertEquals("test@test.nl", dto.getEmail());
        assertEquals("my_user", dto.getName());
    }

}
package nl.jurrevriesen.vodagone.service.dto;

import nl.jurrevriesen.vodagone.data.entity.*;
import org.junit.Test;
import org.mockito.Mockito;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class CompleteAbonnementDtoTest {
    @Test
    public void thatCanCreateFromEntity() throws ParseException {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = dateFormatter.parse("2018-03-22 21:23:01");

        AbonneeAbonnement abonneeAbonnement = Mockito.mock(AbonneeAbonnement.class);
        Abonnement abonnement = Mockito.mock(Abonnement.class);

        Mockito.when(abonneeAbonnement.getId()).thenReturn(1);
        Mockito.when(abonneeAbonnement.getAbonnement()).thenReturn(abonnement);
        Mockito.when(abonnement.getAanbieder()).thenReturn("my_aanbieder");
        Mockito.when(abonnement.getDienst()).thenReturn("my_dienst");
        Mockito.when(abonneeAbonnement.getPrijsPerMaand()).thenReturn(10.00);
        Mockito.when(abonneeAbonnement.getStartDatum()).thenReturn(date);
        Mockito.when(abonneeAbonnement.getVerdubbeling()).thenReturn(Verdubbeling.NIET_BESCHIKBAAR);
        Mockito.when(abonnement.isDeelbaar()).thenReturn(true);
        Mockito.when(abonneeAbonnement.getStatus()).thenReturn(Status.PROEF);


        CompleteAbonnementDto dto = CompleteAbonnementDto.createFromEntity(abonneeAbonnement);

        assertEquals(1, dto.getId());
        assertEquals("my_aanbieder", dto.getAanbieder());
        assertEquals("my_dienst", dto.getDienst());
        assertEquals("€10.00- per maand", dto.getPrijs());
        assertEquals("2018-03-22", dto.getStartDatum());
        assertEquals("niet-beschikbaar", dto.getVerdubbeling());
        assertEquals(true, dto.isDeelbaar());
        assertEquals("proef", dto.getStatus());
    }
}
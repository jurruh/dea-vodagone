package nl.jurrevriesen.vodagone.service.dto;

import nl.jurrevriesen.vodagone.data.entity.Abonnement;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;

public class AbonnementDtoTest {
    @Test
    public void thatCanCreateFromEntity(){

        Abonnement abonnement = Mockito.mock(Abonnement.class);

        Mockito.when(abonnement.getId()).thenReturn(1);
        Mockito.when(abonnement.getDienst()).thenReturn("my_dienst");
        Mockito.when(abonnement.getAanbieder()).thenReturn("my_aanbieder");

        AbonnementDto dto = AbonnementDto.createFromEntity(abonnement);

        assertEquals(1, dto.getId());
        assertEquals("my_dienst", dto.getDienst());
        assertEquals("my_aanbieder", dto.getAanbieder());
    }
}